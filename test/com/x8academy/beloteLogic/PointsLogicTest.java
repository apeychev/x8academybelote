package com.x8academy.beloteLogic;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.x8academy.beloteComponents.PlayingCard;

public class PointsLogicTest {
	
	
	private PointsLogic pointsLogic = new PointsLogic();
	private DeclarationLogic delcarationLogic = new DeclarationLogic();
	
	PlayingCard aceOfClubs = new PlayingCard("AC", 11);
	PlayingCard eightOfClubs = new PlayingCard("8C", 0);
	PlayingCard kingOfDiamonds = new PlayingCard("KD", 4);
	PlayingCard queenOfDiamonds = new PlayingCard("QD", 3);
	PlayingCard	jackOfDiamonds = new PlayingCard("JD", 2);
	PlayingCard jackOfSpades = new PlayingCard("JS", 2);
	PlayingCard nineOfSpades = new PlayingCard("9S", 0);
	PlayingCard queenOfClubs = new PlayingCard("QC", 3);
	PlayingCard	kingOfClubs = new PlayingCard("KC", 4);
	PlayingCard tenOfSpades = new PlayingCard("10S", 10);
	PlayingCard sevenOfHearts = new PlayingCard("7H", 0);
	PlayingCard eightOfHearts = new PlayingCard("8H", 0);
	PlayingCard nineOfHearts = new PlayingCard("9H", 0);
	PlayingCard nineOfDiamonds = new PlayingCard("9D", 0);
	PlayingCard eightOfSpades = new PlayingCard("8S", 0);
	PlayingCard kingOfSpades = new PlayingCard("KS", 4);
	PlayingCard queenOfSpades = new PlayingCard("QS", 2);
	PlayingCard jackOfClubs = new PlayingCard("JC", 2);
	PlayingCard tenOfClubs = new PlayingCard("10C", 10);
	PlayingCard kingOfHearts = new PlayingCard("KH", 4);
	PlayingCard nineOfClubs = new PlayingCard("9C", 0);
	PlayingCard queenOfHearts = new PlayingCard("QH", 3);
	@Test
	public void GivenTwoTeammatesCardsOneBeingEightOtherSevenPlayingAllTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnZero() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(eightOfClubs);
		teamCards.add(sevenOfHearts);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "AT");
		
		assertEquals(0, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingQueenOtherSevenPlayingAllTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnThree() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(queenOfClubs);
		teamCards.add(sevenOfHearts);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "AT");
		
		assertEquals(3, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingQueenOtherKingPlayingAllTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnSeven() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(queenOfClubs);
		teamCards.add(kingOfClubs);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "AT");
		
		assertEquals(7, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingAceOtherTenPlayingAllTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnTwentyOne() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(aceOfClubs);
		teamCards.add(tenOfSpades);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "AT");
		
		assertEquals(21, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingNineOtherQueenPlayingAllTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnSeventeen() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(queenOfClubs);
		teamCards.add(nineOfSpades);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "AT");
		
		assertEquals(17, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsTwoBeingNinePlayingAllTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnTwentyEight() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(nineOfHearts);
		teamCards.add(nineOfSpades);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "AT");
		
		assertEquals(28, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingJackOneNinePlayingAllTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnThirtyFour() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(jackOfDiamonds);
		teamCards.add(nineOfSpades);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "AT");
		
		assertEquals(34, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingJackOtherKingPlayingAllTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnTwentyFour() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(kingOfDiamonds);
		teamCards.add(jackOfDiamonds);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "AT");
		
		assertEquals(24, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsTwoBeingJacksPlayingAllTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnForty() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(jackOfDiamonds);
		teamCards.add(jackOfSpades);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "AT");
		
		assertEquals(40, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsWithSumZeroOneBeingNinePlayingNoTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnZero() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(eightOfClubs);
		teamCards.add(nineOfSpades);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "NT");
		
		assertEquals(0, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingJackOtherNinePlayingNoTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnTwo() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(jackOfSpades);
		teamCards.add(nineOfSpades);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "NT");
		
		assertEquals(2, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingJackOtherQueenPlayingNoTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnFive() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(jackOfSpades);
		teamCards.add(queenOfClubs);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "NT");
		
		assertEquals(5, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingKingOtherTenPlayingNoTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnFourteen() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(kingOfClubs);
		teamCards.add(tenOfSpades);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "NT");
		
		assertEquals(14, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingTenOtherAcePlayingNoTrumpsWhenAddCurrentHandPointsIsInvokedThenReturnTwentyOne() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(aceOfClubs);
		teamCards.add(tenOfSpades);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "NT");
		
		assertEquals(21, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingSevenOtherEightPlayingDiamondsWhenAddCurrentHandPointsIsInvokedThenReturnZero() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(sevenOfHearts);
		teamCards.add(eightOfClubs);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "D");
		
		assertEquals(0, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingSevenOtherNineOfHeartsPlayingDiamondsWhenAddCurrentHandPointsIsInvokedThenReturnZero() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(sevenOfHearts);
		teamCards.add(nineOfHearts);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "D");
		
		assertEquals(0, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingQueenOtherNineOfDiamondsPlayingDiamondsWhenAddCurrentHandPointsIsInvokedThenReturnSeventeen() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(queenOfClubs);
		teamCards.add(nineOfDiamonds);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "D");
		
		assertEquals(17, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingJackOfSpadesOtherNineOfDiamondsPlayingDiamondsWhenAddCurrentHandPointsIsInvokedThenReturnSixteen() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(jackOfSpades);
		teamCards.add(nineOfDiamonds);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "D");
		
		assertEquals(16, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingJackOfSpadesOtherNineOfDiamondsPlayingSpadesWhenAddCurrentHandPointsIsInvokedThenReturnTwenty() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(jackOfSpades);
		teamCards.add(nineOfDiamonds);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "S");
		
		assertEquals(20, sumOfCards);
	}
	
	@Test
	public void GivenTwoTeammatesCardsOneBeingJackOfSpadesOtherNineOfSpadesPlayingSpadesWhenAddCurrentHandPointsIsInvokedThenReturnThirtyFour() {
		List<PlayingCard> teamCards = new ArrayList<>();
		teamCards.add(jackOfSpades);
		teamCards.add(nineOfSpades);
		
		int sumOfCards = pointsLogic.addCurrentHandPoints(teamCards, "S");
		
		assertEquals(34, sumOfCards);
	}
		
	@Test
	public void GivenTwoListsWithDeclarationsWithNothingInEachWhenPointsFromDeclarationsIsInvokedReturnIntArrayWithElements0And0() {
        List<PlayingCard> firstTeam = Arrays
                .asList(new PlayingCard[] { eightOfClubs, queenOfClubs });
        List<PlayingCard> secondTeam = Arrays
                .asList(new PlayingCard[] { sevenOfHearts, queenOfDiamonds });
		List<String> firstTeamDeclarations = delcarationLogic.announceDeclaration(firstTeam, "AT");
		List<String> secondTeamDeclarations = delcarationLogic.announceDeclaration(secondTeam, "AT");
		
		int[] result = pointsLogic.pointsFromDeclarations(firstTeamDeclarations, secondTeamDeclarations);
		
		assertEquals(0, result[0]);
		assertEquals(0, result[1]);
	}
	
	@Test
	public void GivenTwoListsWithDeclarationsWithOnlyCouplesInEachWhenPointsFromDeclarationsIsInvokedReturnIntArrayWithElements20And20() {
        List<PlayingCard> firstTeam = Arrays
                .asList(new PlayingCard[] { kingOfClubs, queenOfClubs });
        List<PlayingCard> secondTeam = Arrays
                .asList(new PlayingCard[] { kingOfDiamonds, queenOfDiamonds });
		List<String> firstTeamDeclarations = delcarationLogic.announceDeclaration(firstTeam, "AT");
		List<String> secondTeamDeclarations = delcarationLogic.announceDeclaration(secondTeam, "AT");
		
		int[] result = pointsLogic.pointsFromDeclarations(firstTeamDeclarations, secondTeamDeclarations);
		
		assertEquals(20, result[0]);
		assertEquals(20, result[1]);
	}
	
	@Test
	public void GivenTwoListsWithDeclarationsWithOnlyCouplesInFirstAndSecondBeingEmptyWhenPointsFromDeclarationsIsInvokedReturnIntArrayWithElements20And0() {
        List<PlayingCard> firstTeam = Arrays
                .asList(new PlayingCard[] { kingOfClubs, queenOfClubs });
        List<PlayingCard> secondTeam = Arrays
                .asList(new PlayingCard[] { kingOfDiamonds, queenOfDiamonds });
		List<String> firstTeamDeclarations = delcarationLogic.announceDeclaration(firstTeam, "C");
		List<String> secondTeamDeclarations = delcarationLogic.announceDeclaration(secondTeam, "C");
		
		int[] result = pointsLogic.pointsFromDeclarations(firstTeamDeclarations, secondTeamDeclarations);
		
		assertEquals(20, result[0]);
		assertEquals(0, result[1]);
	}
	
	@Test
	public void GivenTwoListsWithDeclarationsWithTierceInFirstAndCouplesInSecondWhenPointsFromDeclarationsIsInvokedReturnIntArrayWithElements20And20() {
        List<PlayingCard> firstTeam = Arrays
                .asList(new PlayingCard[] { kingOfClubs, queenOfClubs, aceOfClubs });
        List<PlayingCard> secondTeam = Arrays
                .asList(new PlayingCard[] { kingOfDiamonds, queenOfDiamonds });
		List<String> firstTeamDeclarations = delcarationLogic.announceDeclaration(firstTeam, "D");
		List<String> secondTeamDeclarations = delcarationLogic.announceDeclaration(secondTeam, "D");
		
		int[] result = pointsLogic.pointsFromDeclarations(firstTeamDeclarations, secondTeamDeclarations);
		
		
		assertEquals(20, result[0]);
		assertEquals(20, result[1]);
	}
	
	@Test
	public void GivenTwoListsWithDeclarationsWithTierceWithoutCouplesInFirstAndSecondWhenPointsFromDeclarationsIsInvokedReturnIntArrayWithElements0And0() {
        List<PlayingCard> firstTeam = Arrays
                .asList(new PlayingCard[] { eightOfSpades, nineOfSpades, tenOfSpades });
        List<PlayingCard> secondTeam = Arrays
                .asList(new PlayingCard[] { queenOfClubs, kingOfClubs, aceOfClubs });
		List<String> firstTeamDeclarations = delcarationLogic.announceDeclaration(firstTeam, "S");
		List<String> secondTeamDeclarations = delcarationLogic.announceDeclaration(secondTeam, "S");
		
		int[] result = pointsLogic.pointsFromDeclarations(firstTeamDeclarations, secondTeamDeclarations);
		
		
		assertEquals(0, result[0]);
		assertEquals(0, result[1]);
	}
}
