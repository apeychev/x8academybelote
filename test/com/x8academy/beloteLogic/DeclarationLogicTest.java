package com.x8academy.beloteLogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.x8academy.beloteComponents.Player;
import com.x8academy.beloteComponents.PlayingCard;

public class DeclarationLogicTest {

	DeclarationLogic declarationTest = new DeclarationLogic();
    Player alex = new Player("Alex");

	PlayingCard aceOfClubs = new PlayingCard("AC", 11);
	PlayingCard eightOfClubs = new PlayingCard("8C", 0);
	PlayingCard kingOfDiamonds = new PlayingCard("KD", 4);
	PlayingCard jackOfDiamonds = new PlayingCard("JD", 2);
	PlayingCard nineOfSpades = new PlayingCard("9S", 0);
	PlayingCard queenOfClubs = new PlayingCard("QC", 4);
	PlayingCard kingOfClubs = new PlayingCard("KC", 2);
	PlayingCard tenOfSpades = new PlayingCard("10S", 0);

	@Test
	public void GivenEightCardsWithNoHeartsWhenNumberOfSuitInPlayerHandIsInvokedThenReturnNumberOfSuits() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(aceOfClubs);
		cards.add(eightOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(jackOfDiamonds);
		cards.add(nineOfSpades);
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		int numberOfSuits = declarationTest.numberOfSuitInPlayerHand(cards, "H");

		assertEquals(0, numberOfSuits);
	}

	@Test
	public void GivenEightCardsWhenNumberOfSuitInPlayerHandIsInvokedThenReturnNumberOfSuits() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(aceOfClubs);
		cards.add(eightOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(jackOfDiamonds);
		cards.add(nineOfSpades);
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		int numberOfSuits = declarationTest.numberOfSuitInPlayerHand(cards, "C");

		assertEquals(4, numberOfSuits);
	}

	@Test
	public void GivenEightCardsWithClubsCoupleWhenHasCoupleIsInvokedThenReturnTrue() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(aceOfClubs);
		cards.add(eightOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(jackOfDiamonds);
		cards.add(nineOfSpades);
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean expectedTrue = declarationTest.hasCouples(cards, "C");

		assertTrue(expectedTrue);
	}

	@Test
	public void GivenEightCardsWithNoCoupleWhenHasCoupleIsInvokedThenReturnFalse() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(aceOfClubs);
		cards.add(eightOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(jackOfDiamonds);
		cards.add(nineOfSpades);
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean expectedFalse = declarationTest.hasCouples(cards, "D");

		assertFalse(expectedFalse);
	}

	@Test
	public void GivenEightCardsWithNoCarreWhenHasCarreIsInvokedReturnFalse() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(aceOfClubs);
		cards.add(eightOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(jackOfDiamonds);
		cards.add(nineOfSpades);
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasCarre = declarationTest.hasCarre(cards);

		assertFalse(hasCarre);
	}

	@Test
	public void GivenEightCardsWithCarreOfSevenWhenHasCarreIsInvokedReturnFalse() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("7C", 0));
		cards.add(new PlayingCard("7H", 0));
		cards.add(new PlayingCard("7D", 0));
		cards.add(new PlayingCard("7S", 0));
		cards.add(nineOfSpades);
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasCarre = declarationTest.hasCarre(cards);

		assertFalse(hasCarre);
	}

	@Test
	public void GivenEightCardsWithValidCarreWhenHasCarreIsInvokedReturnTrue() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("AC", 0));
		cards.add(new PlayingCard("AH", 0));
		cards.add(new PlayingCard("AD", 0));
		cards.add(new PlayingCard("AS", 0));
		cards.add(nineOfSpades);
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasCarre = declarationTest.hasCarre(cards);

		assertTrue(hasCarre);
	}

	@Test
	public void GivenEightCardsWithNoCarreOfNinesWhenHasCarreOfNinesIsInvokedReturnFalse() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("AC", 0));
		cards.add(new PlayingCard("AH", 0));
		cards.add(new PlayingCard("AD", 0));
		cards.add(new PlayingCard("AS", 0));
		cards.add(nineOfSpades);
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasCarre = declarationTest.hasCarreOfNines(cards);

		assertFalse(hasCarre);
	}

	@Test
	public void GivenEightCardsWithCarreOfNinesWhenHasCarreOfNinesIsInvokedReturnTrue() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("9C", 0));
		cards.add(new PlayingCard("9H", 0));
		cards.add(new PlayingCard("9D", 0));
		cards.add(new PlayingCard("9S", 0));
		cards.add(new PlayingCard("JD", 2));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasCarre = declarationTest.hasCarreOfNines(cards);

		assertTrue(hasCarre);
	}

	@Test
	public void GivenEightCardsWithNoCarreOfJacksWhenHasCarreOfJacksIsInvokedReturnFalse() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("9C", 0));
		cards.add(new PlayingCard("9H", 0));
		cards.add(new PlayingCard("9D", 0));
		cards.add(new PlayingCard("9S", 0));
		cards.add(new PlayingCard("JD", 2));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasCarre = declarationTest.hasCarreOfJacks(cards);

		assertFalse(hasCarre);
	}

	@Test
	public void GivenEightCardsWithCarreOfJacksWhenHasCarreOfJacksIsInvokedReturnTrue() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("JC", 2));
		cards.add(new PlayingCard("JH", 2));
		cards.add(new PlayingCard("JD", 2));
		cards.add(new PlayingCard("JS", 2));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasCarre = declarationTest.hasCarreOfJacks(cards);

		assertTrue(hasCarre);
	}

	@Test
	public void GivenEightCardsWithNoTierceWhenHasTierceIsInvokedThenReturnFalse() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("8S", 2));
		cards.add(new PlayingCard("JH", 2));
		cards.add(new PlayingCard("JD", 2));
		cards.add(new PlayingCard("JS", 2));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasTierce = declarationTest.hasTierce(cards);

		assertFalse(hasTierce);
	}

	@Test
	public void GivenEightCardsWithTierceWhenHasTierceIsInvokedThenReturnTrue() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("AC", 2));
		cards.add(new PlayingCard("JH", 2));
		cards.add(new PlayingCard("JD", 2));
		cards.add(new PlayingCard("JS", 2));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasTierce = declarationTest.hasTierce(cards);

		assertTrue(hasTierce);

	}

	@Test
	public void GivenEightCardsWithNoQuarteWhenHasQuarteIsInvokedThenReturnFalse() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("AC", 2));
		cards.add(new PlayingCard("JH", 2));
		cards.add(new PlayingCard("JD", 2));
		cards.add(new PlayingCard("JS", 2));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasQuarte = declarationTest.hasQuarte(cards);

		assertFalse(hasQuarte);
	}

	@Test
	public void GivenEightCardsWithQuarteWhenHasQuarteIsInvokedThenReturnTrue() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("AC", 2));
		cards.add(new PlayingCard("JC", 2));
		cards.add(new PlayingCard("JD", 2));
		cards.add(new PlayingCard("JS", 2));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasQuarte = declarationTest.hasQuarte(cards);

		assertTrue(hasQuarte);
	}

	@Test
	public void GivenEightCardsWithNoQuintWhenHasQuintIsInvokedThenReturnFalse() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("AC", 2));
		cards.add(new PlayingCard("JC", 2));
		cards.add(new PlayingCard("JD", 2));
		cards.add(new PlayingCard("JS", 2));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasQuint = declarationTest.hasQuint(cards);

		assertFalse(hasQuint);
	}

	@Test
	public void GivenEightCardsWithQuintOfFiveCardsWhenHasQuintIsInvokedThenReturnTrue() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("AC", 2));
		cards.add(new PlayingCard("JC", 2));
		cards.add(new PlayingCard("10C", 10));
		cards.add(new PlayingCard("JS", 2));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasQuint = declarationTest.hasQuint(cards);

		assertTrue(hasQuint);
	}

	@Test
	public void GivenEightCardsWithQuintOfMoreThanFiveCardsWhenHasQuintIsInvokedThenReturnTrue() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("AC", 2));
		cards.add(new PlayingCard("JC", 2));
		cards.add(new PlayingCard("10C", 10));
		cards.add(new PlayingCard("9C", 0));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		boolean hasQuint = declarationTest.hasQuint(cards);

		assertTrue(hasQuint);
	}

	@Test
	public void GivenEightCardsWithCoupleWhenAnnounceDeclarationIsInvokedReturnStringCouples() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("AS", 2));
		cards.add(new PlayingCard("JH", 2));
		cards.add(new PlayingCard("10C", 10));
		cards.add(new PlayingCard("9D", 0));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(jackOfDiamonds);

		List<String> clubsCouples = declarationTest.announceDeclaration(cards, "C");

		assertEquals("Couples", clubsCouples.get(0));
	}

	@Test
	public void GivenEightCardsWithNoCoupleWhenAnnounceDeclarationIsInvokedReturnEmptyString() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("AS", 2));
		cards.add(new PlayingCard("JH", 2));
		cards.add(new PlayingCard("10C", 10));
		cards.add(new PlayingCard("9D", 0));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		List<String> clubsCouples = declarationTest.announceDeclaration(cards, "D");

		assertTrue(clubsCouples.size() == 0);
	}

	@Test
	public void GivenEightCardsWithCarreOfJacksWhenAnnounceDeclarationIsInvokedReturnString200() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("JS", 2));
		cards.add(new PlayingCard("JH", 2));
		cards.add(new PlayingCard("JC", 10));
		cards.add(new PlayingCard("JD", 0));
		cards.add(new PlayingCard("8D", 0));
		cards.add(aceOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		List<String> carreJacks = declarationTest.announceDeclaration(cards, "D");

		assertEquals("200", carreJacks.get(0));
	}

	@Test
	public void GivenEightCardsWithCarreOfJacksAndCouplesWhenAnnounceDeclarationIsInvokedReturnStringCouples200() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("JS", 2));
		cards.add(new PlayingCard("JH", 2));
		cards.add(new PlayingCard("JC", 10));
		cards.add(new PlayingCard("JD", 0));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		List<String> clubsCouplesJacks = declarationTest.announceDeclaration(cards, "C");

		assertEquals("Couples", clubsCouplesJacks.get(0));
		assertEquals("200", clubsCouplesJacks.get(1));
	}

	@Test
	public void GivenEightCardsWithCarreOfNinesWhenAnnounceDeclarationIsInvokedReturnString150() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("9S", 2));
		cards.add(new PlayingCard("9H", 2));
		cards.add(new PlayingCard("9C", 10));
		cards.add(new PlayingCard("9D", 0));
		cards.add(new PlayingCard("8D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		List<String> carreNines = declarationTest.announceDeclaration(cards, "H");

		assertEquals("150", carreNines.get(0));
	}

	@Test
	public void GivenEightCardsWithCarreOfNinesAndCouplesWhenAnnounceDeclarationIsInvokedReturnStringCouples150() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("9S", 2));
		cards.add(new PlayingCard("9H", 2));
		cards.add(new PlayingCard("9C", 10));
		cards.add(new PlayingCard("9D", 0));
		cards.add(new PlayingCard("10D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(tenOfSpades);

		List<String> clubsCouplesNines = declarationTest.announceDeclaration(cards, "C");

		assertEquals("Couples", clubsCouplesNines.get(0));
		assertEquals("150", clubsCouplesNines.get(1));
	}

	@Test
	public void GivenEightCardsWithCarreWhenAnnounceDeclarationIsInvokedReturnStringCarre() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("10S", 2));
		cards.add(new PlayingCard("10H", 2));
		cards.add(new PlayingCard("10C", 10));
		cards.add(new PlayingCard("9D", 0));
		cards.add(new PlayingCard("10D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(aceOfClubs);

		List<String> carre = declarationTest.announceDeclaration(cards, "H");

		assertEquals("Carre", carre.get(0));
	}

	@Test
	public void GivenEightCardsWithCarreAndCouplesWhenAnnounceDeclarationIsInvokedReturnStringCouplesCarre() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("10S", 2));
		cards.add(new PlayingCard("10H", 2));
		cards.add(new PlayingCard("10C", 10));
		cards.add(new PlayingCard("9D", 0));
		cards.add(new PlayingCard("10D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(aceOfClubs);

		List<String> carreCouples = declarationTest.announceDeclaration(cards, "C");

		assertEquals("Couples", carreCouples.get(0));
		assertEquals("Carre", carreCouples.get(1));
	}

	@Test
	public void GivenEightCardsWithQuintWhenAnnounceDeclarationIsInvokedReturnStringQuint() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("10S", 2));
		cards.add(new PlayingCard("JC", 2));
		cards.add(new PlayingCard("10C", 10));
		cards.add(new PlayingCard("9D", 0));
		cards.add(new PlayingCard("10D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(aceOfClubs);

		List<String> quint = declarationTest.announceDeclaration(cards, "D");

		assertEquals("Quint", quint.get(0));
	}

	@Test
	public void GivenEightCardsWithQuintAndCouplesWhenAnnounceDeclarationIsInvokedReturnStringQuint() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("10S", 2));
		cards.add(new PlayingCard("JC", 2));
		cards.add(new PlayingCard("10C", 10));
		cards.add(new PlayingCard("9D", 0));
		cards.add(new PlayingCard("10D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(aceOfClubs);

		List<String> quintCouples = declarationTest.announceDeclaration(cards, "C");

		assertEquals("Couples", quintCouples.get(0));
		assertEquals("Quint", quintCouples.get(1));
	}

	@Test
	public void GivenEightCardsWithQuarteWhenAnnounceDeclarationIsInvokedReturnStringQuarta() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("7S", 2));
		cards.add(new PlayingCard("8S", 2));
		cards.add(new PlayingCard("9S", 10));
		cards.add(new PlayingCard("10S", 0));
		cards.add(new PlayingCard("10D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(aceOfClubs);

		List<String> quarta = declarationTest.announceDeclaration(cards, "D");

		assertEquals("Quarta", quarta.get(0));
	}

	@Test
	public void GivenEightCardsWithQuarteAndCouplesWhenAnnounceDeclarationIsInvokedReturnStringCouplesQuarta() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("7S", 2));
		cards.add(new PlayingCard("8S", 2));
		cards.add(new PlayingCard("9S", 10));
		cards.add(new PlayingCard("10S", 0));
		cards.add(new PlayingCard("10D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(aceOfClubs);

		List<String> couplesQuarta = declarationTest.announceDeclaration(cards, "C");

		assertEquals("Couples", couplesQuarta.get(0));
		assertEquals("Quarta", couplesQuarta.get(1));
	}

	@Test
	public void GivenEightCardsWithTierceWhenAnnounceDeclarationIsInvokedReturnStringTierce() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("7S", 2));
		cards.add(new PlayingCard("8H", 2));
		cards.add(new PlayingCard("9S", 10));
		cards.add(new PlayingCard("10S", 0));
		cards.add(new PlayingCard("10D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(aceOfClubs);

		List<String> tierce = declarationTest.announceDeclaration(cards, "D");

		assertEquals("Tierce", tierce.get(0));
	}

	@Test
	public void GivenEightCardsWithTierceAndCouplesWhenAnnounceDeclarationIsInvokedReturnStringCouplesTierce() {
		List<PlayingCard> cards = new ArrayList<>();

		cards.add(new PlayingCard("7S", 2));
		cards.add(new PlayingCard("8S", 2));
		cards.add(new PlayingCard("9S", 10));
		cards.add(new PlayingCard("10H", 0));
		cards.add(new PlayingCard("10D", 0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(aceOfClubs);

		List<String> tierceCouples = declarationTest.announceDeclaration(cards, "C");

		assertEquals("Couples", tierceCouples.get(0));
		assertEquals("Tierce", tierceCouples.get(1));
	}

	@Test
	public void GivenEightCardsWithNoDeclarationWhenAnnounceDeclarationIsInvokedReturnEmptyString() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(new PlayingCard("7H", 2));
		cards.add(new PlayingCard("8H", 2));
		cards.add(new PlayingCard("9S", 10));
		cards.add(new PlayingCard("10S", 0));
		cards.add(new PlayingCard("10D",0));
		cards.add(queenOfClubs);
		cards.add(kingOfClubs);
		cards.add(new PlayingCard("AH", 11));
		
		List<String> nothing = declarationTest.announceDeclaration(cards, "D");
		
		assertTrue(nothing.size() == 0);
	}
}
