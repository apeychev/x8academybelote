package com.x8academy.beloteLogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.x8academy.beloteComponents.PlayingCard;

public class BiddingLogicTest {
	
	BiddingLogic biddingTest = new BiddingLogic();
	
	PlayingCard aceOfClubs = new PlayingCard("AC", 11);
	PlayingCard eightOfClubs = new PlayingCard("8C", 0);
	PlayingCard kingOfDiamonds = new PlayingCard("KD", 4);
	PlayingCard	jackOfDiamonds = new PlayingCard("JD", 2);
	PlayingCard nineOfSpades = new PlayingCard("9S", 0);
	PlayingCard queenOfClubs = new PlayingCard("QC", 4);
	PlayingCard	kingOfClubs = new PlayingCard("KC", 2);
	PlayingCard tenOfSpades = new PlayingCard("10S", 0);
	
	@Test
	public void GivenCardsForBiddingSuitAndCurrentBidIsPassWhenBidSuitIsInvokedThenReturnTheBiddingSuit() {
		biddingTest.setCurrentBid("pass");
		List<PlayingCard> cards = new ArrayList<>();
		cards.add(jackOfDiamonds);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(queenOfClubs);
		cards.add(eightOfClubs);
		
		String suitDiamonds = biddingTest.bidSuit(cards);
		
		assertEquals("D", suitDiamonds);
	}
	
	@Test
	public void GivenCardsForBiddingAndCurrentBidIsLowerSuitWhenBidSuitIsInvokedThenReturnTheGivenBiddingSuit() {
		biddingTest.setCurrentBid("D");
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(aceOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(nineOfSpades);
		cards.add(eightOfClubs);
		
		String suitSpades = biddingTest.bidSuit(cards);
		
		assertEquals("S", suitSpades);
	}
	
	@Test
	public void GivenCardsForBiddingAndCurrentBidIsHigherSuitWhenBidSuitIsInvokedThenReturnTheCurrentBiddingSuit() {
		biddingTest.setCurrentBid("D");
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(aceOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(new PlayingCard("JC", 2));
		cards.add(eightOfClubs);
		
		String suitClubs = biddingTest.bidSuit(cards);
		
		assertEquals("pass", suitClubs);
	}
	
	@Test
	public void GivenCardsForPassWhenBidSuitIsInvokedThenReturnPass() {
		biddingTest.setCurrentBid("D");
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(aceOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(new PlayingCard("7H", 0));
		cards.add(eightOfClubs);
		
		String pass = biddingTest.bidSuit(cards);
		
		assertEquals("pass", pass);
	}
	
	@Test
	public void GivenCardsForNoTrumpsWhenBidNoTrumpIsInvokedThenReturnTrue() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(aceOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(new PlayingCard("7H", 0));
		cards.add(eightOfClubs);
		
		boolean noTrumps = biddingTest.bidNoTrumps(cards);
		
		assertTrue(noTrumps);
	}
	
	@Test
	public void GivenCardsNotForNoTrumpsAndCurrentBidIsSuitWhenBidNoTrumpIsInvokedThenReturnFalse() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(queenOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(new PlayingCard("7H", 0));
		cards.add(eightOfClubs);
		
		boolean noTrumps = biddingTest.bidNoTrumps(cards);
		
		assertFalse(noTrumps);
	}
	
	@Test
	public void GivenCardsForAllTrumpsWhenBidAllTrumpsIsInvokedThenReturnTrue() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(jackOfDiamonds);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(new PlayingCard("9S", 0));
		cards.add(eightOfClubs);
		
		boolean allTrumps = biddingTest.bidAllTrumps(cards);
		
		assertTrue(allTrumps);
	}
	
	@Test
	public void GivenCardsNotForAllTrumpsWhenBidAllTrumpsIsInvokedThenReturnFalse() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(queenOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(new PlayingCard("9S", 0));
		cards.add(eightOfClubs);
		
		boolean allTrumps = biddingTest.bidAllTrumps(cards);
		
		assertFalse(allTrumps);
	}
	
	@Test
	public void GivenCardsForPassWhenPlayerBidIsInvokedThenReturnPass() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(queenOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(new PlayingCard("8S", 0));
		cards.add(eightOfClubs);
		
		String pass = biddingTest.playerBid(cards);
		
		assertEquals("pass", pass);
	}
	
	@Test
	public void GivenCardsOnlyForSuitWhenPlayerBidIsInvokedThenReturnSuit() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(queenOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(new PlayingCard("JS", 0));
		cards.add(eightOfClubs);
		
		String spades = biddingTest.playerBid(cards);
		
		assertEquals("S", spades);
	}
	
	@Test
	public void GivenCardsForSuitAndNoTrumpWhenPlayerBidIsInvokedThenReturnNoTrump() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(aceOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(new PlayingCard("JS", 0));
		cards.add(eightOfClubs);
		
		String noTrumps = biddingTest.playerBid(cards);
		
		assertEquals("NT", noTrumps);
	}
	
	@Test
	public void GivenCardsForSuitAndAllTrumpsWhenPlayerBidIsInvokedThenReturnAllTrumps() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(aceOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(tenOfSpades);
		cards.add(new PlayingCard("JS", 0));
		cards.add(new PlayingCard("9H", 0));
		
		String allTrumps = biddingTest.playerBid(cards);
		
		assertEquals("AT", allTrumps);
	}
	
	@Test
	public void GivenCardsForNoTrumpsAndAllTrumpsWhenPlayerBidIsInvokedThenReturnAllTrumps() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(aceOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(new PlayingCard("10D", 10));
		cards.add(new PlayingCard("JS", 0));
		cards.add(nineOfSpades);
		
		String allTrumps = biddingTest.playerBid(cards);
		
		assertEquals("AT", allTrumps);
	}
	
	@Test
	public void GivenCardsForAllTrumpsWhenPlayerBidIsInvokedThenReturnAllTrumps() {
		List<PlayingCard> cards = new ArrayList<>();
		
		cards.add(eightOfClubs);
		cards.add(kingOfDiamonds);
		cards.add(new PlayingCard("10D", 10));
		cards.add(new PlayingCard("JS", 0));
		cards.add(nineOfSpades);
		
		String allTrumps = biddingTest.playerBid(cards);
		
		assertEquals("AT", allTrumps);
	}
	
	
}
