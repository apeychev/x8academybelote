package com.x8academy.beloteComponents;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PlayerTest {
	
    private Player alex = new Player("Alex");
	
	@Test
	public void GivenObjectPlayerWhenGetNameIsInvokedThenReturnName() {
		
        String playerName = alex.getName();
		
        assertEquals("Alex", playerName);
	}
	
	@Test
	public void GivenFivePlayingCardsWhenReceiveCardIsInvokedThenCheckIfCardsAreStored() {
		PlayingCard aceOfClubs = new PlayingCard("AC", 11);
		PlayingCard eightOfClubs = new PlayingCard("8C", 0);
		PlayingCard kingOfDiamonds = new PlayingCard("KD", 4);
		PlayingCard	jackOfHearts = new PlayingCard("JH", 2);
		PlayingCard nineOfSpades = new PlayingCard("9S", 0);
		
        alex.receiveCard(nineOfSpades);
        alex.receiveCard(jackOfHearts);
        alex.receiveCard(kingOfDiamonds);
        alex.receiveCard(eightOfClubs);
        alex.receiveCard(aceOfClubs);
		
        int cardsInHand = alex.getCardsInHand().size();
		
		assertEquals(5, cardsInHand);
	}

}
