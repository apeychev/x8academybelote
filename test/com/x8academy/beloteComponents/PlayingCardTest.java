package com.x8academy.beloteComponents;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PlayingCardTest {
	
	private PlayingCard aceOfSpades = new PlayingCard("AS", 11);
	private PlayingCard tenOfHearts = new PlayingCard("10H", 10);
	
	@Test
	public void GivenObjectPlayingCardWhenGetNameIsInvokedThenReturnCardName() {
		
		String cardName = aceOfSpades.getName();
		
		assertEquals("AS", cardName);
	}
	
	@Test
	public void GivenObjectPlayingCardWithTwoLettersRankWhenGetRankIsInvokedThenReturnCardRank() {
		
		String cardRank = tenOfHearts.getRank();
		
		assertEquals("10", cardRank);
	}
	
	@Test
	public void GivenObjectPlayingCardWithOneLetterRankWhenGetRankIsInvokedThenReturnCardRank() {
		
		String cardRank = aceOfSpades.getRank();
		
		assertEquals("A", cardRank);
	}
	
	
	@Test
	public void GivenObjectPlayingCardWithTwoLetterRankWhenGetSuitIsInvokedThenReturnCardSuit() {
		
		String cardSuit = tenOfHearts.getSuit();
		
		assertEquals("H", cardSuit);
	}
	
	@Test
	public void GivenObjectPlayingCardWithOneLetterRankWhenGetSuitIsInvokedThenReturnCardSuit() {
		
		String cardSuit = aceOfSpades.getSuit();
		
		assertEquals("S", cardSuit);
	}
	
	@Test
	public void GivenObjectPlayingCardWhenGetPointsIsInvokedThenReturnCardPoints() {
		
		int cardPoints = aceOfSpades.getPoints();
		
		assertEquals(11, cardPoints);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void GivenInvalidCardNameWhenCreatingPlayingCardObjectThenIllegalArgumentExceptionIsThrown() {
		PlayingCard invalidCard = new PlayingCard("OS", 542);
	}
}
