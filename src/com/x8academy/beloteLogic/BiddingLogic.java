package com.x8academy.beloteLogic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.x8academy.beloteComponents.PlayingCard;

public class BiddingLogic {

    private final static Map<String, Integer> ORDER_OF_SUITS = new HashMap<>();

    static {
        ORDER_OF_SUITS.put("pass", 0);
        ORDER_OF_SUITS.put("C", 1);
        ORDER_OF_SUITS.put("D", 2);
        ORDER_OF_SUITS.put("H", 3);
        ORDER_OF_SUITS.put("S", 4);
        ORDER_OF_SUITS.put("NT", 5);
        ORDER_OF_SUITS.put("AT", 6);
        ORDER_OF_SUITS.put("x2", 7);
        ORDER_OF_SUITS.put("x4", 8);
    }

	private String currentBid = "pass";

	public void setCurrentBid(String currentBid) {
		this.currentBid = currentBid;
	}

	private Map<String, List<PlayingCard>> groupCardsBySuit(List<PlayingCard> playerCards) {
		Map<String, List<PlayingCard>> groupBySuit = new HashMap<>();
		List<PlayingCard> temporaryList;
		
		for (PlayingCard currentCard : playerCards) {
			if (groupBySuit.containsKey(currentCard.getSuit())) {
				temporaryList = groupBySuit.get(currentCard.getSuit());
				temporaryList.add(currentCard);

				groupBySuit.put(currentCard.getSuit(), temporaryList);
			} else {
				List<PlayingCard> singleCardList = new ArrayList<>();
				singleCardList.add(currentCard);
				groupBySuit.put(currentCard.getSuit(), singleCardList);
			}
		}
		return groupBySuit;
	}

	public String bidSuit(List<PlayingCard> playerCards) {
		Map<String, List<PlayingCard>> suitCards = this.groupCardsBySuit(playerCards);
		String localBid = "pass";

		for (Map.Entry<String, List<PlayingCard>> entry : suitCards.entrySet()) {
			
			if (goodToCallSuit(entry.getValue())) {
				localBid = entry.getKey();
			}
		}

		if (ORDER_OF_SUITS.get(currentBid) < ORDER_OF_SUITS.get(localBid)) {
			this.currentBid = localBid;
		} else {
			localBid = "pass";
		}
		
		return localBid;
	}

	private boolean goodToCallSuit(List<PlayingCard> cards) {
		int counter = 0;
		
		if (cards.size() < 1) {
			return false;
		}
		
		for (int i = 0; i < cards.size(); ++i) {
			if (cards.get(i).getRank().equals("J") ||
				cards.get(i).getRank().equals("9")) {
				counter++;
			}
		}
		
		return counter >= 1 ? true : false;
	}
	
	public boolean bidNoTrumps(List<PlayingCard> playerCards) {
		int counterOfHighCards = 0;
		
		for (int i = 0; i < playerCards.size(); ++i) {
			if (playerCards.get(i).getRank().equals("A") ||
				playerCards.get(i).getRank().equals("10")) {
				counterOfHighCards++;
			}
		}
		
		return counterOfHighCards >= 2 ? true : false;
	}
	
	public boolean bidAllTrumps(List<PlayingCard> playerCards) {
		int counterOfHighCards = 0;
		
		for (int i = 0; i < playerCards.size(); ++i) {
			if (playerCards.get(i).getRank().equals("J") ||
				playerCards.get(i).getRank().equals("9")) {
				counterOfHighCards++;
			}
		}
		
		return counterOfHighCards >= 2 ? true : false;
	}
	
	//so far i will not include x2, x4 bids because the logic is more difficult
	public String playerBid(List<PlayingCard> playerCards) {
		String bid = "pass";
		
		
		//if (! this.bidSuit(playerCards).equals("pass")) {
			
		bid = this.bidSuit(playerCards);
		//}
		
		if (this.bidNoTrumps(playerCards)) {
			bid = "NT";
		}
		
		if (this.bidAllTrumps(playerCards)) {
			bid = "AT";
		}
		
		return bid;
	}
	
}
