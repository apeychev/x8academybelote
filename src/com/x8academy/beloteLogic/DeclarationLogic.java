package com.x8academy.beloteLogic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.x8academy.beloteComponents.PlayingCard;

public class DeclarationLogic {

	private final static String TIERCE = "Tierce";
	private final static String QUARTA = "Quarta";
	private final static String QUINT = "Quint";
	private final static String COUPLES = "Couples";
	private final static String CARRE = "Carre";
	private final static String NINES_150 = "150";
	private final static String JACKS_200 = "200";

    private final static Map<String, Integer> CARD_ORDER = new HashMap<>();

    static {
        CARD_ORDER.put("7", 1);
        CARD_ORDER.put("8", 2);
        CARD_ORDER.put("9", 3);
        CARD_ORDER.put("10", 4);
        CARD_ORDER.put("J", 5);
        CARD_ORDER.put("Q", 6);
        CARD_ORDER.put("K", 7);
        CARD_ORDER.put("A", 8);

    }

	private int numberOfCouples = 0;

	public int numberOfSuitInPlayerHand(List<PlayingCard> playerCards, String suit) {
		int occurrencesOfSuit = 0;

		for (PlayingCard currentCard : playerCards) {
			if (currentCard.getSuit().equals(suit)) {
				occurrencesOfSuit++;
			}
		}
		return occurrencesOfSuit;
	}

	public boolean hasCouples(List<PlayingCard> playerCards, String trump) {
		List<String> kingSuits = new ArrayList<>();
		List<String> queenSuits = new ArrayList<>();
		numberOfCouples = 0;

		for (PlayingCard currentCard : playerCards) {

			if (trump.equals("AT") || currentCard.getSuit().equals(trump)) {
				if (currentCard.getRank().equals("K")) {
					kingSuits.add(currentCard.getSuit());
				} else if (currentCard.getRank().equals("Q")) {
					queenSuits.add(currentCard.getSuit());
				}
			}
		}
		
		for (int i = 0; i < kingSuits.size(); ++i) {
			if (queenSuits.contains(kingSuits.get(i))) {
				numberOfCouples++;
			}
		}
		
		return numberOfCouples > 0 ? true : false;
	}

	private Map<String, Integer> countRankOccurences(List<PlayingCard> playerCards) {
		Map<String, Integer> occurrencesOfRank = new HashMap<>();

		for (PlayingCard currentCard : playerCards) {
			if (currentCard.getRank().equals("7") || currentCard.getRank().equals("8")) {
				continue;
			}

			if (occurrencesOfRank.containsKey(currentCard.getRank())) {
				occurrencesOfRank.put(currentCard.getRank(), occurrencesOfRank.get(currentCard.getRank()) + 1);
			} else {
				occurrencesOfRank.put(currentCard.getRank(), 1);
			}
		}

		return occurrencesOfRank;
	}

	private Map<String, List<PlayingCard>> groupCardsBySuit(List<PlayingCard> playerCards) {
		Map<String, List<PlayingCard>> groupBySuit = new HashMap<>();
		List<PlayingCard> temporaryList;

		for (PlayingCard currentCard : playerCards) {
			if (groupBySuit.containsKey(currentCard.getSuit())) {
				temporaryList = groupBySuit.get(currentCard.getSuit());
				temporaryList.add(currentCard);

				groupBySuit.put(currentCard.getSuit(), temporaryList);
			} else {
				List<PlayingCard> singleCardList = new ArrayList<>();
				singleCardList.add(currentCard);
				groupBySuit.put(currentCard.getSuit(), singleCardList);
			}
		}
		return groupBySuit;
	}

	public boolean hasCarre(List<PlayingCard> playerCards) {
		Map<String, Integer> occurrencesOfRank = this.countRankOccurences(playerCards);

		for (Map.Entry<String, Integer> entry : occurrencesOfRank.entrySet()) {
			if (entry.getValue() == 4) {
				return true;
			}
		}
		return false;
	}

	public boolean hasCarreOfNines(List<PlayingCard> playerCards) {
		Map<String, Integer> occurrencesOfRank = this.countRankOccurences(playerCards);

		for (Map.Entry<String, Integer> entry : occurrencesOfRank.entrySet()) {
			if (entry.getKey().equals("9") && entry.getValue() == 4) {
				return true;
			}
		}
		return false;
	}

	public boolean hasCarreOfJacks(List<PlayingCard> playerCards) {
		Map<String, Integer> occurrencesOfRank = this.countRankOccurences(playerCards);

		for (Map.Entry<String, Integer> entry : occurrencesOfRank.entrySet()) {
			if (entry.getKey().equals("J") && entry.getValue() == 4) {
				return true;
			}
		}
		return false;
	}

	public boolean hasTierce(List<PlayingCard> playerCards) {
		Map<String, List<PlayingCard>> groupBySuit = this.groupCardsBySuit(playerCards);

		for (List<PlayingCard> currentCards : groupBySuit.values()) {
			if (currentCards.size() < 3) {
				continue;
			} else if (numberConsecutiveCards(currentCards) == 3) {
				return true;
			}
		}
		return false;
	}

	public boolean hasQuarte(List<PlayingCard> playerCards) {
		Map<String, List<PlayingCard>> groupBySuit = this.groupCardsBySuit(playerCards);

		for (List<PlayingCard> currentCards : groupBySuit.values()) {
			if (currentCards.size() < 4) {
				continue;
			} else if (numberConsecutiveCards(currentCards) == 4) {
				return true;
			}
		}
		return false;
	}

	public boolean hasQuint(List<PlayingCard> playerCards) {
		Map<String, List<PlayingCard>> groupBySuit = this.groupCardsBySuit(playerCards);
	
		for (List<PlayingCard> currentCards : groupBySuit.values()) {
			if (currentCards.size() < 5) {
				continue;
			} else if (numberConsecutiveCards(currentCards) >= 5) {
				return true;
			}
		}
		return false;
	}

	private int numberConsecutiveCards(List<PlayingCard> cards) {
		List<Integer> orderOfEachCard = new ArrayList<>();

		for (PlayingCard currentCard : cards) {
			orderOfEachCard.add(CARD_ORDER.get(currentCard.getRank()));
		}

		Collections.sort(orderOfEachCard);

		int numberConsecutiveCards = 1;
		for (int i = 0; i < orderOfEachCard.size() - 1; ++i) {
			if (orderOfEachCard.get(i) + 1 == orderOfEachCard.get(i + 1)) {
				numberConsecutiveCards++;
			}
		}
		return numberConsecutiveCards;
	}

	public List<String> announceDeclaration(List<PlayingCard> playerCards, String trumpCard) {
		List<String> declaration = new ArrayList<>();

		if (trumpCard.equals("NT")) {
			declaration.add("");
			return declaration;
		}

		if (!(trumpCard.equals("NT")) && hasCouples(playerCards, trumpCard)) {
			for (int i = 0; i < numberOfCouples; i++) {
				declaration.add(COUPLES);
			}
		}

		if (hasCarreOfJacks(playerCards)) {
			declaration.add(JACKS_200);
			return declaration;
		}

		if (hasCarreOfNines(playerCards)) {
			declaration.add(NINES_150);
			return declaration;
		}

		if (hasCarre(playerCards)) {
			declaration.add(CARRE);
			return declaration;
		}

		if (hasQuint(playerCards)) {
			declaration.add(QUINT);
			return declaration;
		} else if (hasQuarte(playerCards)) {
			declaration.add(QUARTA);
			return declaration;
		} else if (hasTierce(playerCards)) {
			declaration.add(TIERCE);
			return declaration;
		}
		// declaration.add("");
		return declaration;
	}
}
