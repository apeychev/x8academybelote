package com.x8academy.beloteLogic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.x8academy.beloteComponents.PlayingCard;

public class PointsLogic {

	private final static int FIRST_TEAM_POINTS = 0;
	private final static int SECOND_TEAM_POINTS = 1;
	private final static int FIRST_TEAMMATE_CARD = 0;
	private final static int SECOND_TEAMMATE_CARD = 1;
	private final static int NUMBER_OF_TEAMMATES = 2;

    private final static Map<String, Integer> ALL_TRUMPS_POINTS = new HashMap<>();

    private final static Map<String, Integer> DECLARATION_POINTS = new HashMap<>();

    static {
        ALL_TRUMPS_POINTS.put("7", 0);
        ALL_TRUMPS_POINTS.put("8", 0);
        ALL_TRUMPS_POINTS.put("9", 14);
        ALL_TRUMPS_POINTS.put("10", 10);
        ALL_TRUMPS_POINTS.put("J", 20);
        ALL_TRUMPS_POINTS.put("Q", 3);
        ALL_TRUMPS_POINTS.put("K", 4);
        ALL_TRUMPS_POINTS.put("A", 11);

        DECLARATION_POINTS.put("", 0);
        DECLARATION_POINTS.put("Tierce", 20);
        DECLARATION_POINTS.put("Couples", 20);
        DECLARATION_POINTS.put("Quarta", 50);
        DECLARATION_POINTS.put("Quint", 100);
        DECLARATION_POINTS.put("Carre", 100);
        DECLARATION_POINTS.put("150", 150);
        DECLARATION_POINTS.put("200", 200);
    }

	private int sumPointsAllTrumps(List<PlayingCard> currentCards) {
		int currentResult = 0;

		currentResult += ALL_TRUMPS_POINTS.get(currentCards.get(FIRST_TEAMMATE_CARD).getRank());
		currentResult += ALL_TRUMPS_POINTS.get(currentCards.get(SECOND_TEAMMATE_CARD).getRank());

		return currentResult;
	}

	private int sumPointsNoTrumps(List<PlayingCard> currentCards) {
		int currentResult = 0;

		currentResult += currentCards.get(FIRST_TEAMMATE_CARD).getPoints();
		currentResult += currentCards.get(SECOND_TEAMMATE_CARD).getPoints();

		return currentResult;
	}

	private int sumPointsSuit(List<PlayingCard> currentCards, String trumpCard) {
		int currentResult = 0;

		for (int i = 0; i < NUMBER_OF_TEAMMATES; ++i) {
			if (currentCards.get(i).getSuit().equals(trumpCard)
					&& (currentCards.get(i).getRank().equals("9") || currentCards.get(i).getRank().equals("J"))) {
				currentResult += ALL_TRUMPS_POINTS.get(currentCards.get(i).getRank());
			} else {
				currentResult += currentCards.get(i).getPoints();
			}
		}
		return currentResult;
	}

	public int addCurrentHandPoints(List<PlayingCard> currentCards, String trumpCard) {

		if (trumpCard.equals("AT")) {
			return this.sumPointsAllTrumps(currentCards);
		}

		if (trumpCard.equals("NT")) {
			return this.sumPointsNoTrumps(currentCards);
		}

		return this.sumPointsSuit(currentCards, trumpCard);

	}

	// NOTE : due to high complexity of following the rules as they are I am making
	// a slight change in the way I am computing declarations :
	// the team with higher amount of overall points from declarations keeps them &
	// the other team loses them;
	// both teams keep points from couples
	public int[] pointsFromDeclarations(List<String> firstTeamDeclarations, List<String> secondTeamDeclarations) {
		int firstTeamPoints = 0;
		int firstCouplesPoints = 0;
		int secondTeamPoints = 0;
		int secondCouplesPoints = 0;
		int[] pointsForBothTeams = new int[2];

		firstCouplesPoints = this.accumulateCouplesPoints(firstTeamDeclarations, pointsForBothTeams, FIRST_TEAM_POINTS);
		secondCouplesPoints = this.accumulateCouplesPoints(secondTeamDeclarations, pointsForBothTeams,
				SECOND_TEAM_POINTS);

		firstTeamPoints = this.accumulateDeclarationsPoints(firstTeamDeclarations);
		secondTeamPoints = this.accumulateDeclarationsPoints(secondTeamDeclarations);

		if (firstTeamPoints == secondTeamPoints) {
			if ((firstCouplesPoints  == secondCouplesPoints)) {
				return pointsForBothTeams;
			}
		}

		if (firstTeamPoints + firstCouplesPoints > secondTeamPoints) {
			pointsForBothTeams[FIRST_TEAM_POINTS] += firstTeamPoints;
		} else if (firstTeamPoints < secondTeamPoints + secondCouplesPoints) {
			pointsForBothTeams[SECOND_TEAM_POINTS] += secondTeamPoints;
		}

		return pointsForBothTeams;
	}

	private int accumulateCouplesPoints(List<String> teamDeclarations, int[] pointsForBothTeams, int whichTeam) {
		int couplesPoints = 0;
		int team;

		if (whichTeam == FIRST_TEAM_POINTS) {
			team = FIRST_TEAM_POINTS;
		} else {
			team = SECOND_TEAM_POINTS;
		}

		for (int i = 0; i < teamDeclarations.size(); i++) {

			if (teamDeclarations.get(i).equals("Couples")) {
				pointsForBothTeams[team] += DECLARATION_POINTS.get("Couples");
				couplesPoints += DECLARATION_POINTS.get("Couples");
			}
		}

		return couplesPoints;
	}

	private int accumulateDeclarationsPoints(List<String> declarations) {
		int points = 0;

		for (int i = 0; i < declarations.size(); ++i) {
			if (!(declarations.get(i).equals("Couples"))) {
				points += DECLARATION_POINTS.get(declarations.get(i));
			}
		}

		return points;
	}

}
