package com.x8academy.beloteComponents;

import java.util.ArrayList;

public class Player {
	
	private static final int MAX_CARDS_PER_PLAYER = 8;
	
	private ArrayList<PlayingCard> cardsInHand;
	private String name;
	
	public Player(String name) {
		this.name = name;
        cardsInHand = new ArrayList<PlayingCard>(MAX_CARDS_PER_PLAYER);
	}
	
	public String getName() {
		return this.name; 
	}
	
	public ArrayList<PlayingCard> getCardsInHand() {
		return this.cardsInHand;
	}
	
	public void receiveCard(PlayingCard cardName) {
		
		cardsInHand.add(cardName);
	}
	
	
}
