package com.x8academy.beloteComponents;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class PlayingCard {

    private static final String[] TEMP_ARR_OF_VALID_CARDS = new String[] { "7C", "7D", "7H", "7S", "8C", "8D",
            "8H", "8S", "9C", "9D", "9H", "9S", "10C", "10D", "10H", "10S", "JC", "JD", "JH", "JS",
            "QC", "QD", "QH", "QS", "KC", "KD", "KH", "KS", "AC", "AD", "AH", "AS" };
    private static final Set<String> VALID_CARDS = new HashSet<String>(Arrays.asList(TEMP_ARR_OF_VALID_CARDS));

	private String name;
	private String rank;
	private String suit;
	private int points;

	public PlayingCard(String name, int points) {
		this.setName(name);
		this.setRank(name);
		this.setSuit(name);
		this.setPoints(points);
	}
	
	public void setName(String name) {
		if (!VALID_CARDS.contains(name)) {
			throw new IllegalArgumentException("This is not a valid card name.");
		}
		
		this.name = name;
	}

	public void setRank(String name) {
		if (name.length() == 2) {
			this.rank = name.substring(0, 1);
		} else {
			this.rank = name.substring(0, 2);
		}
	}

	public void setSuit(String name) {
		if (name.length() == 2) {
			this.suit = name.substring(1, 2);
		} else {
			this.suit = name.substring(2, 3);
		}
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public String getName() {
		return this.name;
	}

	public String getRank() {
		return this.rank;
	}

	public String getSuit() {
		return this.suit;
	}

	public int getPoints() {
		return this.points;
	}
}
